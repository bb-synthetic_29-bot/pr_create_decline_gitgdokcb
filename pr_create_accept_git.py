import os
import subprocess
import sys
import time

# noinspection PyUnresolvedReferences,PyUnboundLocalVariable
check = check  # noqa

ROOT_DIR = '/tmp'
SSH_DIR = ROOT_DIR + '/sshome'
TESTKEY_ID = SSH_DIR + '/id_testkey'
KNOWN_HOSTS = SSH_DIR + '/known_hosts'


def setup_hostkeys():
    """Sets up the SSH host keys for bitbucket.org in known_hosts"""
    cmd = 'ssh-keyscan bitbucket.org'
    hostkeys = subprocess.check_output(cmd.split(), stderr=subprocess.STDOUT)
    with open(KNOWN_HOSTS, 'w') as known_hosts:
        known_hosts.write(hostkeys)


def setup_sshkey():
    """Decodes the SSH private key and places it in the proper directory"""
    SSH_KEY = check.secrets.get('SSH_KEY')

    if not SSH_KEY:
        check.fail('The SSH_KEY setting is missing. This should be a base64 '
                   'encoded SSH private key with access to the '
                   'target repository.')

    os.open(TESTKEY_ID, os.O_CREAT, 0700)
    with open(TESTKEY_ID, 'w') as keyfile:
        keyfile.write(SSH_KEY)


def setup_utils():
    repo_url = 'git@bitbucket.org:bitbucket-puppet/synthetic-check-utils.git'
    ssh_cmd = (
        'ssh -o UserKnownHostsFile=%s -i %s' % (KNOWN_HOSTS, TESTKEY_ID))
    repo = '%s/repo/' % ROOT_DIR
    subprocess.check_output(
        ['git', 'clone', repo_url, repo],
        stderr=subprocess.STDOUT,
        env={'GIT_SSH_COMMAND': ssh_cmd})
    sys.path.append(repo)


os.makedirs(SSH_DIR)
setup_hostkeys()
setup_sshkey()
setup_utils()

import pollinator_util as pout  # noqa
import synth_util as sut  # noqa

################
# BEGIN SCRIPT #
################

pr_data = {
    'title': 'temp',
    'source': {
        'branch': {
            'slug': 'temp'
        },
        'commit': {
            'commit': 'TODO: CHANGEME'
        }
    },
    'destination': {
        'branch': {
            'slug': 'master'
        },
        'commit': {
            'commit': 'TODO: CHANGEME'
        }
    }
}

with pout.forked_repo('imcd/pr_create_accept_git', pout.USER,
                      (pout.USER, pout.PASSWORD)) as endpoint:
    prs = endpoint / 'pullrequests'
    new_pr = prs / prs.post(pr_data)['id']
    pr_data.update({'state': 'APPROVED'})
    time.sleep(3)
    new_pr.post(pr_data)
